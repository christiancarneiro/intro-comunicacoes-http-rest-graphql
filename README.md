# Intro - Comunicações - HTTP REST GRAPHQL

Primeira atividade de backEnd. Treinamento NavalSub.
Buscador de imagens via API https://list.ly/api/docs#meta-search-search-images

## How to

1. Rodar ./src/main2.js.
2. inserir termo de busca (em inglês) no prompt de comando, conforme solicitado.

- um arquivo json (../written_json/termo_de_busca.json) com dados acerca das imagens encontradas da API é escrito.
- em seguida, é perguntado se o usuário deseja ou não baixar as imagens encontradas.

3. Caso a resposta seja 'sim':
- até 12 imagens encontradas (../written_images/termo_de_busca_index.jpg) serão baixadas.

4. Caso a resposta seja 'não':
- As imagens encontradas terão seus url individuais dispostos no Prompt de comando.

- obs: Caso não sejam encontradas imagens, o usuário é notificado do problema.

## Example

1. inserindo o termo de busca "mountain"
2. O arquivo ../written_json/mountain.json é escrito
3. dando 'sim' à permissão solicitada
4. os arquivos ../written_images/mountain index.jpg são escritos.

este exemplo encontra-se disposto nos arquivos do repositório.



