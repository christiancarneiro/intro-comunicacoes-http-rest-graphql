const { inputItem } = require("../inputData");
const { getImages, getData } = require("../utils/getData");
const { outputImages } = require("../outputData");

// recebe o texto da pesquisa, faz o request, recebe as respostas, filtra as imagens, imprime links para as imagens.
main = async () => {
  const search = await inputItem();
  const resultsResponse = await getData(search["item"]);
  const imageResponse = getImages(resultsResponse);
  outputImages(imageResponse);
};

main();
