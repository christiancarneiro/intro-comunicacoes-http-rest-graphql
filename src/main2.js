const { inputItem, inputVerification } = require("../utils/inputData");
const { getImages, getUrl } = require("../utils/getData");
const { writeJson, writeImage } = require("../utils/writeData");
const { readJson } = require("../utils/readData");
const { outputImages } = require("../utils/outputData");

/* recebe o texto da pesquisa, faz o request, recebe as respostas, recebe um nome de arquivo
escreve um json com as respostas, lê o json, obtem as respostas, filtra as imagens, salva as imagens */
// CHUVA DE AWAIT
main = async () => {
  const search = await inputItem();
  const url = getUrl(search["item"]);
  const jsonPath = await writeJson(search["item"], url);
  const jsonData = await readJson(jsonPath);
  const jsonImages = getImages(jsonData);
  const numImages = jsonImages.length;
  if (numImages > 0) {
    const verification = await inputVerification(numImages);
    if (verification["permission"] === "sim") {
      jsonImages.forEach((image, index) =>
        writeImage(`${search["item"]} ${index}`, image)
      );
    } else {
      console.log("\nEntão tá bom... toma os links: ");
      outputImages(jsonImages);
    }
  } else {
    console.log(`Não foram encontradas images de ${search.item}`);
  }
};

main();
