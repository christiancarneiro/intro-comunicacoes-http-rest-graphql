const { inputItem, inputFileName } = require("../utils/inputData");
const { getData, getImages } = require("../utils/getData");
const { writeJson } = require("../utils/writeData");
const { readJson } = require("../utils/readData");
const { outputImages } = require("../utils/outputData");

/* recebe o texto da pesquisa, faz o request, recebe as respostas, recebe um nome de arquivo
escreve um json com as respostas, lê o json, obtem as respostas, filtra as imagens, imprime links para as imagens */
// CHUVA DE AWAIT
main = async () => {
  const search = await inputItem();
  const resultResponse = await getData(search["item"]);
  const filePrefix = await inputFileName();
  const filePath = writeJson(filePrefix["name"], resultResponse);
  const fileData = readJson(filePath);
  const resultImages = getImages(fileData);
  outputImages(resultImages);
};

main();
