const axios = require("axios");

// auxiliares
const getUrl = (search) => {
  return `https://list.ly/api/v4/search/image?q=${search}`;
};
// ------

// pega os dados completos da API
const getData = async (search) => {
  const url = getUrl(search);
  console.log(
    `\nbuscando as melhores imagens de ${search} do mundo graças a Deus...`
  );

  const response = await axios.get(url).catch((err) => {
    console.log(`\nreceba este erro: ${err}`);
    throw err;
  });
  return response.data.results;
};

// filtra apenas as imagens
const getImages = (results) => {
  let responseImage;
  if (results) {
    responseImage = results.map((item) => item.image);
  }
  return responseImage;
};

module.exports = { getData, getImages, getUrl };
