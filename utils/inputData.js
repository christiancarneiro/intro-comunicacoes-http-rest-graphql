prompt = require("prompt");

inputItem = async () => {
  console.log("\nO que procuras? (in english)");
  return await prompt.get(["item"]);
};

inputVerification = async (numImages) => {
  console.log(
    `\nVocê permite que ${numImages} imagens sejam baixadas em sua máquina? "sim/não"`
  );
  return await prompt.get(["permission"]);
};

module.exports = { inputItem, inputVerification };
