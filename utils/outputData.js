outputImages = (imagesData) => {
  if (imagesData && imagesData.length > 0) {
    console.log("\nAqui estão as imagens encontradas:");
    imagesData.forEach((element, id) =>
      console.log(`\nimagem ${id}.\n${element}`)
    );
  } else {
    console.log("\nHouve um erro ao encontrar imagens. Tente novamente.");
  }
};

module.exports = { outputImages };
