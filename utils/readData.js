const fs = require("fs");

const readJson = (jsonPath) => {
  try {
    const jsonString = fs.readFileSync(jsonPath, "utf-8");
    const jsonData = JSON.parse(jsonString);
    return jsonData;
  } catch (err) {
    console.log(err);
  }
};

module.exports = { readJson };
