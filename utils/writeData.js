const fs = require("fs");
const axios = require("axios");

// escreve um json baseado no Data, retorna o nome do arquivo.
const writeJson = async (jsonName, url) => {
  const response = await axios
    .get(url)
    .catch((err) => console.log("Erro no request", err));
  if (response) {
    const dataString = JSON.stringify(response.data.results, null, 2);
    const jsonPath = `../written_json/${jsonName}.json`;
    try {
      fs.writeFileSync(jsonPath, dataString);
      console.log(`\nArquivo "${jsonName}.json" salvo pelo goleiro do Luva!`);
      return jsonPath;
    } catch (err) {
      console.log(`Erro ao escrever ${jsonPath}`, err);
    }
  }
};

const writeImage = async (imageName, url) => {
  const imagePath = "../written_images/" + imageName + ".jpg";
  const response = await axios({
    method: "get",
    url: url,
    responseType: "arraybuffer",
  }).catch((err) => console.log("Erro no request", err));
  if (response) {
    try {
      fs.writeFileSync(imagePath, Buffer.from(response.data, "binary"));
      console.log(`Imagem "${imageName}" salva pelo goleiro do Luva!`);
      return imagePath;
    } catch (err) {
      console.log(`Erro ao escrever "${imagePath}"`, err);
    }
  }
};
module.exports = { writeJson, writeImage };
